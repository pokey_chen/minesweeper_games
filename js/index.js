function Mine(tr,td,mineNum){
    // 用户传
    this.tr = tr;//行数
    this.td = td;//列数
    this.mineNum = mineNum;//雷数量


    // 自己定义
    this.squares = [];//存储所有方块的信息，他是一个二维数组，按行和列的顺序排序，存取都使用行列的形式
    this.tds = [];//存储所有单元格的DOM(二维数组)
    this.surplusMine = mineNum;//剩余雷的数量
    this.allRight = false;//右击小图标是否全是雷,用来判断用户是否游戏成功

    this.parent = document.querySelector('.gameBox');
}
//生产N个不重复随机数
Mine.prototype.randomNum = function () {
    var square = new Array(this.tr * this.td);//生产一个空数组有长度为格子的总数
    for(var i = 0; i<square.length ; i++){
        square[i] = i;
    }
    square.sort(function () {return 0.5-Math.random()});

        
    return square.slice(0,this.mineNum);//截取99个数随机数
};
Mine.prototype.init = function () {

    var rn = this.randomNum();//雷在格子里的位置
    var n=0;//用来找到格子对应的索引

    for(var i = 0;i<this.tr;i++){
        this.squares[i] = [];//存储所有格子的信息
        for(var j = 0; j<this.td;j++){
            // this.squares[i][j] = ;
            // n++;

//去一个方块在数据要使用行与列的形式去取，找方块周围的时候要用坐标的形式去取。行与列的形式跟坐标的形式x,y刚好是相反的
            if(rn.indexOf(++n) != -1){
                //如果这个条件成立，说明现在循环到的这个索引在雷的数组里找到了，那就表示这个索引对应的是个雷
                this.squares[i][j] = {type:'mine',x:j,y:i};
            }else{
                this.squares[i][j] = {type:'number',x:j,y:i,value:0};
            }



            /*
            {
                type:'mine' 表示雷
                x:0坐标放入
                y:0
            }
            {
                type:'number' 数字会覆盖
                x:
                y:
                value:
            }

             */
        }
    }

    this.parent.oncontextmenu = function () {
        return false;
    }


    this.updateNum();
    this.createDom();

    //剩余雷数
    this.mineNumDome = document.querySelector('.mineNum');
    this.mineNumDome.innerHTML = this.surplusMine;
};

//创建表格
Mine.prototype.createDom = function(){
    var This = this;
    var table = document.createElement('table');

    for(var i=0; i<this.tr; i++){//行循环
        var domTr = document.createElement('tr');
        this.tds[i] = [];

        for(var j=0; j<this.td; j++){//列
            var domTd = document.createElement('td');
            // domTd.innerHTML = 0;
            this.tds[i][j] = domTd;//这里是把所有创建的td添加到数组当中

            domTd.pos = [i,j];//把格子对应的行与列存到格子身上，为了下面通过这个值去数组里取到对应的数据
            domTd.onmousedown = function () {
                This.play(event,this);//This指的是实例对象，this指的是点击的td
            };
            // if(this.squares[i][j].type == 'mine'){
            //     domTd.className = 'mine';

            // }
            // if(this.squares[i][j].type == 'number'){
            //     domTd.innerHTML = this.squares[i][j].value;
                
            // }


            domTr.appendChild(domTd);

        }
        table.appendChild(domTr);

    }

    this.parent.innerHTML = '';//避免多次点击创建多个表
    this.parent.appendChild(table);

}

//找某个格子周围的八个格子
Mine.prototype.getAround = function (square) {
    var x = square.x;
    var y = square.y;
    var result = [];//把找到的格子的坐标返回出去（二维数组)
/*
        x-1,y-1   x,y-1   x+1,y-1
        x-1,y     x,y     x+1,y
        x-1,y+1   x,y+1   x+1,y+1
 */

 //通过坐标去循环九宫格
    for(var i = x-1 ;i<=x+1 ;i++){
        for( var j = y-1;j<=y+1 ;j++){
            if(
                i<0 ||   //格子超出了左边的范围
                j<0 ||   //格子超出了上边的范围
                i> this.td - 1 ||  //格子超出了右边的范围
                j> this.tr - 1 ||  //格子超出了下边的范围
                (i==x && j==y) ||   //当前循环到的格子是自己
                this.squares[j][i].type == 'mine'   //周围的格子是个雷
            ){
                continue;
            }
            result.push([j,i]); //要已行与列的形式返回出去
        }
    }
    return result;
};
//更新所有数字
Mine.prototype.updateNum = function () {
    for(var i = 0;i <this.tr; i++){
        for(var j=0; j<this.td; j++){
            //只更新的是雷周围的数字
            if(this.squares[i][j].type == 'number'){
                continue;
            }

            var num = this.getAround(this.squares[i][j]);//获取到每一个雷周围的数字

            for(var k = 0;k<num.length; k++){
                // num[k] == [0,1]
                // num[k][0] == 0;
                // num[k][1] == 1;

                this.squares[num[k][0]][num[k][1]].value += 1;
            }

        }
    }
}

Mine.prototype.play = function (ev,obj) {
        var This = this;
        if(ev.which == 1 && obj.className != 'flag'){//右面是为了限制标了小红旗不能左键点击
            //如果点击的是左键
            var curSquare = this.squares[obj.pos[0]][obj.pos[1]];
            var cl = ['zero','one','two','three','four','five','six','seven','eight'];
            
            if(curSquare.type == 'number'){
                //用户点到的是数字
                obj.innerHTML = curSquare.value;
                obj.className = cl[curSquare.value];

                if(curSquare.value == 0){
                    /*
                        点到了数字0(递归)
                            1、显示自己
                            2、找四周
                               1、显示四周(如果四周的值不为0就显示到这里)
                               2、如果值为0，
                                  1、显示自己
                                  2、找四周(如果四周的值不为0就显示到这里)
                                      1、显示自己
                                      2、找四周
                    */
                   obj.innerHTML = '';//如果数字为0不显示 1、

                   function getAllZero(square){//找四周
                    var around = This.getAround(square);//找到了周围n个格子

                    for(var i=0;i<around.length ;i++){
                        // around[i] = [0,0]
                        var x = around[i][0];//行
                        var y = around[i][1];//列

                        This.tds[x][y].className = cl[This.squares[x][y].value];

                        if(This.squares[x][y].value == 0){
                            //如果以某个格子为中心找到的格子值为0，那就需要接着调用函数（递归）
                            if(!This.tds[x][y].check){
                                //给对应的tds添加一个属性，这条属性用于决定这个格子有没有被找过，如果被找过的话，他值就为true，下一次就不会再找了
                                This.tds[x][y].check = true;
                                getAllZero(This.squares[x][y]);
                            }
                        }else{
                            //如果以某个格子为中心四周找到的不为0，就把人家的数据显示出来
                            This.tds[x][y].innerHTML = This.squares[x][y].value;
                        }

                    }
                   }
                    getAllZero(curSquare);
                   
                }
            }else{
                // console.log('雷');
                this.gameOver(obj);
            }
        }

        //用户点击是右键
        if(ev.which == 3){
            //如果点击的是数字那就不能点击
            if(obj.className && obj.className != 'flag'){
                return;
            }
            obj.className = obj.className == 'flag'?'':'flag';//三步运算符，切换class

            if(this.squares[obj.pos[0]][obj.pos[1]].type == 'mine'){
                this.allRight = true;//用户表的小红旗背后都是雷
            }else{
                this.allRight = false;
            }

            if(obj.className == 'flag'){
                this.mineNumDome.innerHTML = --this.surplusMine;
            }else{
                this.mineNumDome.innerHTML = ++this.surplusMine;//点的是有旗的位置
            }

            if(this.surplusMine == 0){
                //剩余的雷的数量为0，表示用户已经标完小红旗了，这时候是判断游戏是成功还是失败
                if(this.allRight){
                    //这个条件成立说明用户全部标对了
                    alert('游戏通过');
                }else{
                    alert('游戏失败');
                    this.gameOver();
                }
            }
        }
};

//游戏失败函数
Mine.prototype.gameOver = function (clickTd){
    /**
     * 三步
     * 1、显示所有的雷
     * 2、停止页面所有点击功能
     * 3、点到的雷加红
     */

     for(var i=0; i<this.tr; i++){
         for(var j=0;j<this.td; j++){
             if(this.squares[i][j].type == 'mine'){
                 //显示所有雷
                 this.tds[i][j].className = 'mine';
             }

             this.tds[i][j].onmousedown = null;
         }
     }
    if(clickTd){
        clickTd.style.backgroundColor = '#f00';
    }
}

//上面button按钮功能
var btns = document.querySelectorAll('.level button');
var mine = null;//存储生成的实例
var ln = 0; //用来处理当前选中状态
var arr = [[9,9,10],[16,16,40],[28,28,99]];//不同级别

for(let i=0;i<btns.length-1 ; i++){
    btns[i].onclick = function(){
        btns[ln].className = '';
        this.className = 'active';

        mine = new Mine(...arr[i]);//es6 
        mine.init();

        ln = i;
    }
}
btns[0].onclick();//初始化一下
btns[3].onclick = function () {
    mine.init();
    
}
// var mine = new Mine(9,9,10);
// mine.init();